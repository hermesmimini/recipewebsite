package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import data.DataAccessInterface;
import models.Credentials;
import models.Recipe;

/**
 * This is the business service for the Recipe model, all the logic that
 * connects the controller and the database is used in here.
 * 
 * @author Hermes Mimini
 * @author Isaiah Camacho
 *
 */
@Stateless
@Local(RecipeBusinessInterface.class)
@Alternative
public class RecipeBusinessService implements RecipeBusinessInterface {

	// Inject the recipeDataService
	@Inject
	DataAccessInterface<Recipe> recipeDataService;

	/**
	 * Default Constructor
	 */
	public RecipeBusinessService() {
	}

	/**
	 * This method will utilize the recipeDataService and return a list of all the
	 * recipes in the database.
	 * 
	 * @return list - List(Type Recipe) Class (List of all recipes in the database.)
	 */
	@Override
	public List<Recipe> getRecipes() {
		// call the recipeDataService to return the recipe list
		return recipeDataService.findAll();
	}

	/**
	 * This method will take in a recipe object and will add it to the database,
	 * after the recipe is added it will return a Boolean.
	 * 
	 * @param recipe - Recipe Class (Recipe that will be added to the database.)
	 * @return Boolean Class - (Boolean value depending on the result of the
	 *         dataService.)
	 */
	@Override
	public Boolean onAdd(Recipe recipe, int uniqueId) {
		// call the create dataService function
		if (recipeDataService.create(recipe, uniqueId) == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method will delete the recipe that is sent to it, it will grab the
	 * recipe ID and use the dataService to delete the recipe. Once complete a
	 * boolean object is going to be returned.
	 * 
	 * @param recipe - Recipe Class (Recipe that will be removed from the database.)
	 * @return Boolean Class - (Boolean value depending on the result of the
	 *         dataService.)
	 */
	@Override
	public Boolean onDelete(Recipe recipe) {
		// call the create dataService function
		if (recipeDataService.delete(recipe) == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method will update the content of the recipe by using the
	 * recipeDataService.
	 * 
	 * @param recipe - Recipe Class (Recipe that will be updated in the database.)
	 * @return Boolean Class - (Boolean value depending on the result of the
	 *         dataService.)
	 */
	@Override
	public Boolean onUpdate(Recipe recipe, int recipeId) {
		// call the create dataService function
		if (recipeDataService.update(recipe, recipeId) == 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * This method will utilize the recipeDataService and return a list of recipes
	 * in the database that are unique to the user that is calling it.
	 * 
	 * @return list - List(Type Recipe) Class (List of recipes from the database that
	 *         are related to the user in session.)
	 */
	@Override
	public List<Recipe> getMyRecipes() {
		// Set the session and get the credentials from the session
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		Credentials c = (Credentials) session.getAttribute("userCred");

		// Grab all the recipes from the database by using the dataService
		List<Recipe> recipes = recipeDataService.findAll();

		// Create a new list that will store the recipes of the current user
		List<Recipe> returnRecipes = new ArrayList<Recipe>();

		// Loop through the recipes
		for (int i = 0; i < recipes.size(); i++) {
			// if the recipe user ID equals the current user ID, add
			if (recipeDataService.getUniqueId(recipes.get(i)) == c.getID()) {
				returnRecipes.add(recipes.get(i));
			}
		}
		return returnRecipes;
	}

}
