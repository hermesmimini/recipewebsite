package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import business.RecipeBusinessInterface;
import models.Credentials;
import models.Recipe;

/**
 * This is the controller class for the Recipe where it handles the front end
 * page distribution, the methods in this class accept (or don't accept) data
 * and based on the outcome the methods will return a string of the page it is
 * directed.
 * 
 * @author Hermes Mimini
 * @author Isaiah Camacho
 *
 */
@ManagedBean
@ViewScoped
public class RecipeController {

	// Inject beans
	@Inject
	RecipeBusinessInterface recipeService;

	/**
	 * This method will add a recipe to the database by calling the businessService,
	 * the UserId will be grabbed from the session variable and be used to add the
	 * recipe so that recipe has a direct connection to the user who added it.
	 * 
	 * @param recipe - Recipe Class (Recipe model that will be added to the
	 *               database)
	 * @return String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onAdd(Recipe recipe) {
		// Create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		// check if the session is null
		// grab the credentials from the session
		Credentials c = (Credentials) session.getAttribute("userCred");
		try {
			// call the business service to add the recipe
			if (recipeService.onAdd(recipe, c.getID()) == true) {
				// return to the list of users recipes
				return "myRecipes.xhtml";
			} else {
				// return error
				return "error.xhtml";
			}
			// catch SQL error with custom exception
		} catch (Exception e) {
			return "error.xhtml";
		}
	}

	/**
	 * This method will delete the recipe sent to it from the database by calling
	 * the recipeBusinessService.
	 * 
	 * @param recipe - Recipe Class (Recipe that the user wants to delete)
	 * @return String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onDelete(Recipe recipe) {

		// clearing the recipe in the session by putting a default one, so details wont
		// display the
		// recipe in the session variable
		// create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		Recipe r = new Recipe();
		session.setAttribute("recipe", r);
		try {
			// call the recipeBusinessService to delete recipe
			if (recipeService.onDelete(recipe) == true) {
				// return users recipes
				return "myRecipes.xhtml";
			} else {
				// return error message
				return "error.xhtml";
			}
			// catch SQL error with custom exception
		} catch (Exception e) {
			// return error message
			return "error.xhtml";
		}

	}

	/**
	 * This method will call the confirmation delete page and set the recipe sent to
	 * the session.
	 * 
	 * @param recipe - Recipe Class (Recipe that the user wants to delete)
	 * @return - String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onDeleteConfirmation(Recipe recipe) {
		// Create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		// putting the recipe in the session
		session.setAttribute("recipe", recipe);
		// call the deleteConfirmation page
		return "deleteConfirmation.xhtml";

	}

	/**
	 * This method will display the information of and individual recipe.
	 * 
	 * @param recipe - Recipe Class (Recipe of which the information is going to be
	 *               displayed)
	 * @return - String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onDisplayRecipe(Recipe recipe) {
		// create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		// check to see if session userCred is empty
		if (session.getAttribute("userCred") == null) {
			// send user to login
			return "loginForm.xhtml";
		} else {
			// put recipe on the page
			FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("recipe", recipe);
			return "displayRecipe.xhtml";
		}

	}

	/**
	 * This method is the first step of a two step process to edit the recipe. In
	 * this method the recipe that the user wants to edit will be put in the session
	 * so it can be grabbed in the onUpdate() method and it will be pre-populated in
	 * the form.
	 * 
	 * @param recipe - Recipe Class (Recipe that the user wants to edit.)
	 * @return String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onEdit(Recipe recipe) {

		// create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);

		// set the id and recipe object to the session
		session.setAttribute("recipeId", recipe.getID());
		// put the recipe that should be prepopulated in the session
		session.setAttribute("recipeEdit", recipe);

		return "editRecipeForm.xhtml";

	}

	/**
	 * This method is the second step of a two step process to edit the recipe. In
	 * this method the recipeBusinessService will be called to edit the recipe.
	 * 
	 * @param recipe - Recipe Class (Recipe that the user wants to edit.)
	 * @return String Class - (Name of the page that the web-site will direct to.)
	 */
	public String onUpdate(Recipe recipe) {

		// create session variable
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		// grab the recipeId that the user wants to edit
		int recipeId = (int) session.getAttribute("recipeId");
		try {
			// call the business service to edit the recipe
			if (recipeService.onUpdate(recipe, recipeId) == true) {
				// set the recipe in the session variable to null
				// so it wont pre-populate again with this recipe.
				Recipe r = new Recipe();
				session.setAttribute("recipe", r);
				// return users recipes
				return "myRecipes.xhtml";
			} else {
				// return error page
				return "error.xhtml";
			}
			// catch SQL error by using custom exception
		} catch (Exception e) {
			return "error.xhtml";
		}

	}

	/**
	 * This method will return the RecipeBusinessInterface
	 * 
	 * @return - RecipeBusinessInterface Class (Recipe Service)
	 */
	public RecipeBusinessInterface getRecipeService() {
		return recipeService;
	}
}
