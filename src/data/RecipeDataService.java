package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import exception.DataServiceException;
import models.Recipe;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class RecipeDataService implements DataAccessInterface<Recipe> {

	// Instantiate connection code
	Connection conn = null;
	String url = "jdbc:mysql://localhost:3306/recipeapp";
	String username = "root";
	String password = "root";

	/**
	 * This method will return a list of all the recipes in the database that belong
	 * to the user.
	 *
	 * @return list - List(Type Recipe) Class - List of all recipes in the database.
	 */
	@Override
	public List<Recipe> findAll() {
		// create SQL statement to receive all recipes from database
		String sql = "SELECT * FROM RECIPES";
		// create a recipes list
		List<Recipe> recipes = new ArrayList<Recipe>();

		try {
			conn = DriverManager.getConnection(url, username, password);
			// create a statement
			Statement stmt = conn.createStatement();
			// run the SQL statement and store the result set
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set, create a new Recipe based on the information in
			// the result set and add it to the list
			while (rs.next()) {
				recipes.add(new Recipe(rs.getInt("ID"), rs.getString("NAME"), rs.getString("DESCRIPTION"),
						rs.getString("INGREDIENTS"), rs.getString("NUTRITIONAL_INFORMATION"), rs.getInt("PRICE")));
			}
		} catch (SQLException e) {
			// print stack trace
			e.printStackTrace();
			// throw custom exception
			throw new DataServiceException(e);
		} finally {
			if (conn != null) {
				try {
					// close connection
					conn.close();
				} catch (SQLException e) {
					// print stack trace
					e.printStackTrace();
				}
			}
		}
		return recipes;
	}

	/**
	 * This method will write the given recipe to the recipe table.
	 * 
	 * @param recipe - Recipe Class (Recipe that will be added in the database.)
	 * @return Integer Class (The value that results from executeUpdate() to see if
	 *         there was actual change in the database.)
	 */
	@Override
	public int create(Recipe recipe, int uniqueId) {
		// create a rowsAffected integer
		int rowsAffected = -1;
		// create the SQL statement to add a recipe to the database
		String sql = String.format(
				"INSERT INTO `recipes` (`NAME`, `DESCRIPTION`, `INGREDIENTS`, `NUTRITIONAL_INFORMATION`, `PRICE`, `users_ID`)"
						+ " VALUES ('%s', '%s', '%s', '%s', %f, %d);",
				recipe.getName(), recipe.getDescription(), recipe.getIngredients(), recipe.getNutritionalInformation(),
				recipe.getPrice(), uniqueId);

		try {
			// connect to the database
			conn = DriverManager.getConnection(url, username, password);
			// create a statement
			Statement stmt = conn.createStatement();
			// run the SQL statement and store the result of execute update
			rowsAffected = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// print stack tree
			e.printStackTrace();
			// throw custom exception
			throw new DataServiceException(e);
		} finally {
			if (conn != null) {
				try {
					// close connection
					conn.close();
				} catch (SQLException e) {
					// print stack tree
					e.printStackTrace();
				}
			}
		}
		// return
		return rowsAffected;
	}

	/**
	 * This method will update the recipe. It takes in the content of the given
	 * recipe and the recipeId of the recipe that is going to be updated.
	 * 
	 * @param recipe - Recipe Class
	 * @return Integer Class (The value that results from executeUpdate() to see if
	 *         there was actual change in the database.)
	 */
	@Override
	public int update(Recipe recipe, int recipeId) {
		// create a rowsAffected integer
		int rowsAffected = -1;
		// create the SQL statement to edit a recipe in the database
		String sql = String.format(
				"UPDATE `recipes` SET `NAME` = '%s', `DESCRIPTION` = '%s', `INGREDIENTS` = '%s', `NUTRITIONAL_INFORMATION` = '%s', `PRICE` = %f WHERE `recipes`.`ID` = %d;",
				recipe.getName(), recipe.getDescription(), recipe.getIngredients(), recipe.getNutritionalInformation(),
				recipe.getPrice(), recipeId);

		try {
			// connect to the database
			conn = DriverManager.getConnection(url, username, password);
			// create a statement
			Statement stmt = conn.createStatement();
			// run the SQL statement and store the result of execute update
			rowsAffected = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// print stack tree
			e.printStackTrace();
			// throw custom exception
			throw new DataServiceException(e);
		} finally {
			if (conn != null) {
				try {
					// close connection
					conn.close();
				} catch (SQLException e) {
					// print stack tree
					e.printStackTrace();
				}
			}
		}
		return rowsAffected;
	}

	/**
	 * This method will delete recipe that is given by using the ID variable
	 * (because it is unique)
	 * 
	 * @param recipe - Recipe Class
	 * @return Integer Class (The value that results from executeUpdate() to see if
	 *         there was actual change in the database.)
	 */
	@Override
	public int delete(Recipe recipe) {
		// create a rowsAffected integer
		int rowsAffected = -1;
		// create the SQL statement to delete a recipe from the database
		String sql = String.format("DELETE FROM `recipes` WHERE `recipes`.`ID` = %d", recipe.getID());

		try {
			// connect to the database
			conn = DriverManager.getConnection(url, username, password);
			// create a statement
			Statement stmt = conn.createStatement();
			// run the SQL statement and store the result of execute update
			rowsAffected = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// print stack tree
			e.printStackTrace();
			// throw custom exception
			throw new DataServiceException(e);
		} finally {
			if (conn != null) {
				try {
					// close connection
					conn.close();
				} catch (SQLException e) {
					// print stack tree
					e.printStackTrace();
				}
			}
		}
		return rowsAffected;
	}

	/**
	 * This method will return the fk ID of the variable.
	 * 
	 * @param t - Recipe Class (Recipe that the method will find the FK of.)
	 * @return Integer Class - (Foreign Key of the recipe)
	 */
	@Override
	public int getUniqueId(Recipe t) {
		// create the SQL statement to grab the FK from the recipe
		String sql = String.format("SELECT `recipes`.`users_ID` FROM `recipes` WHERE `recipes`.`ID` = %d", t.getID());
		// create a uniqueId integer
		int uniqueId = -1;
		try {
			// connect to the database
			conn = DriverManager.getConnection(url, username, password);
			// create a statement
			Statement stmt = conn.createStatement();
			// store result of running the SQL query in result set
			ResultSet rs = stmt.executeQuery(sql);
			// loop through the result set and grab the FK
			while (rs.next()) {
				uniqueId = rs.getInt("users_ID");
			}
		} catch (SQLException e) {
			// print stack tree
			e.printStackTrace();
			// throw custom exception
			throw new DataServiceException(e);
		} finally {
			if (conn != null) {
				try {
					// close connection
					conn.close();
				} catch (SQLException e) {
					// print stack tree
					e.printStackTrace();
				}
			}
		}
		return uniqueId;
	}

	/**
	 * Later implemented
	 */
	@Override
	public Recipe findById(int id) {
		return null;
	}
}
