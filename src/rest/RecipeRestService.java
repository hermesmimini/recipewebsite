package rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import business.RecipeBusinessInterface;
import models.Recipe;

@RequestScoped
@Path("/recipes")
public class RecipeRestService {

	// Inject the RecipeBusinessInterface
	@Inject
	RecipeBusinessInterface recipeService;

	/**
	 * This is a method that will call getRecipes() method from the
	 * recipeBusinessService, which will return a list of all recipes in the
	 * database and will be displayed online with JSON format.
	 * 
	 * @return list - List(Type Recipe)
	 */
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Recipe> getRecipesAsJson() {
		return recipeService.getRecipes();
	}

}
