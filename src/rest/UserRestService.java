package rest;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import business.UserBusinessInterface;
import models.User;

@RequestScoped
@Path("/users")
public class UserRestService {
	
	//Inject the UserBusinessInterface
	@Inject
	UserBusinessInterface userService;

	/**
	 * This is a method that will call getUsers() method from the userBusinessService,
	 * which will return a list of all users in the database and will be displayed 
	 * online with JSON format.
	 * 
	 * @return list - List(Type User)
	 */
	@GET
	@Path("/getjson")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getUsersAsJson() {
		return userService.getUsers();
	}

}
